import { Component, OnInit } from '@angular/core';
import {TramitesService} from '../tramites.service';

@Component({
  selector: 'app-tramites',
  templateUrl: './tramites.component.html',
  styleUrls: ['./tramites.component.css']
})
export class TramitesComponent implements OnInit {
  protected tramites;
  constructor(protected tramitesService: TramitesService) {
    this.tramites = tramitesService.get_tramites().slice(0, 4);
    console.log('tramites: ', this.tramites);
  }

  ngOnInit(): void {

  }

}
