import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TramitesService } from './tramites.service';
import { TramitesComponent } from './tramites/tramites.component';
import { SitiosinteresComponent } from './sitiosinteres/sitiosinteres.component';

@NgModule({
  declarations: [
    AppComponent,
    TramitesComponent,
    SitiosinteresComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [TramitesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
