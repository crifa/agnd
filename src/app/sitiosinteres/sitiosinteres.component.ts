import { Component, OnInit } from '@angular/core';
import {TramitesService} from '../tramites.service';

@Component({
  selector: 'app-sitiosinteres',
  templateUrl: './sitiosinteres.component.html',
  styleUrls: ['./sitiosinteres.component.css']
})
export class SitiosinteresComponent implements OnInit {
  protected data;
  constructor(protected tramitesService: TramitesService) {
    this.data = tramitesService.get_temas_interes();
  }

  ngOnInit(): void {
  }

}
