import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TramitesService {
  data = [];
  data_interes = [];
  constructor() { }

  // tslint:disable-next-line:typedef
  get_tramites() {
    this.data = [{
      nombre: 'Legalización de documentos de educación superior para adelantar estudios o trabajar en el exterior',
      entidad: 'Ministerio de Educación',
      costo: 1
    },
      {
        nombre: 'Adopción de un niño, niña o adolescente por persona, cónyuges o compañeros permanentes residentes en Colombia',
        entidad: 'Ministerio de Justicia',
        costo: 0
      },
      {
        nombre: 'Agendamiento de citas para consultorio jurídico',
        entidad: 'Ministerio de Educación',
        costo: 0
      },
      {
        nombre: 'Certificados y constancias académicas',
        entidad: 'Servicio Nacional de Aprendizaje - SENA',
        costo: 0
      }
    ];
    return this.data;
  }

  // tslint:disable-next-line:typedef
  get_temas_interes() {
    this.data_interes = [{
      nombre: '¿Sabes qué son los datos abiertos y cómo usarlos?',
      descripcion: 'La información que producen las entidades públicas a tu alcance',
      imagen: 'assets/images/datos.png'
    },
      {
        nombre: 'Conoce más sobre nuestro país',
        descripcion: 'Colombia es hermosa y queremos que conozcas más sobre ella',
        imagen: 'assets/images/colombia.png'
      },
      {
        nombre: 'Este portal está pensado para ti',
        descripcion: 'GOV.CO nace para facilitarle a los ciudadanos la interacción con el Estado',
        imagen: 'assets/images/record.png'
      },
      {
        nombre: 'Destacado que esté en acondicionado',
        descripcion: 'Ministerio del interior',
        imagen: 'assets/images/datos.png'
      }
    ];
    return this.data_interes;
  }
}
