import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SitiosinteresComponent } from './sitiosinteres.component';

describe('SitiosinteresComponent', () => {
  let component: SitiosinteresComponent;
  let fixture: ComponentFixture<SitiosinteresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SitiosinteresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SitiosinteresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
